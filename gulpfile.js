var gulp = require('gulp');
var sass = require('gulp-sass');
var inject = require('gulp-inject');
var bowerFiles = require('main-bower-files');
var del = require('del');
var removeHtmlComments = require('gulp-remove-html-comments');
var preprocess = require('gulp-preprocess');
var concat = require('gulp-concat');

gulp.task('default', ['developer']);


var bowerOverridesJS = [
    "./bower_components/angular-i18n/angular-locale_pt-br.js"
];

//Developer
gulp.task('developer', ['bower-files', 'includeBowerOverridesJS', 'developer-sass', 'copy-js', 'copy-html', 'copy-images'], function () {
    return gulp.src('./src/index.html')
        .pipe(inject(gulp.src('dist/**/*.css', { read: false }), { relative: true, ignorePath: '../dist/' }))
        .pipe(inject(gulp.src(['dist/scripts/angular.js', 'dist/scripts/jquery.js', 'dist/scripts/**/*.js'], { read: false }), { relative: true, ignorePath: '../dist/' }))
        .pipe(inject(gulp.src(["!dist/scripts/**/*.js", 'dist/**/*.js'], { read: false }), { relative: true, name: 'app', ignorePath: '../dist/' }))
        .pipe(removeHtmlComments())
        .pipe(gulp.dest('./dist'));
});


gulp.task('bower-files', ['clean-dist'], function () {
    return gulp.src(bowerFiles())
        .pipe(gulp.dest('./dist/scripts'))
});


gulp.task('includeBowerOverridesJS', ['clean-dist'], function () {
    return gulp.src(bowerOverridesJS)
        .pipe(gulp.dest('./dist/services'));
});

gulp.task('developer-sass', ['clean-dist'], function () {

    var files = bowerFiles('**/*.css');
    files.push('./src/**/*.scss');

    return gulp.src(files)
        .pipe(sass({
            includePaths: ['bower_components/foundation-sites/scss']
        }))
        .pipe(preprocess({ context: { PATH: '../../' } }))
        .pipe(gulp.dest('./dist'));
});

gulp.task('copy-js', ['clean-dist'], function () {
    return gulp.src(['src/**/*.js'])
        .pipe(gulp.dest('./dist'));
});

gulp.task('copy-html', ['clean-dist'], function () {
    return gulp.src(['./src/**/*.html'])
        .pipe(gulp.dest('./dist'));
});

gulp.task('copy-images', ['clean-dist'], function() {
    return gulp.src('./src/assets/images/*.{png,gif,jpg,html,svg}')
        .pipe(gulp.dest('./dist/assets/images'));
});

gulp.task('clean-dist', function () {
    return del([
        'dist/**/*'
    ]);
});

