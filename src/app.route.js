
App.config(function($routeProvider, $locationProvider) {
    $routeProvider
        .when('/lista-fundos-de-investimento/', {
            controller: 'InvestmentFundsController',
            templateUrl: 'apps/investment-funds/investment-funds.view.html',
            params:{
                title:"Lista de Fundos de Investimento",
                subTitle:"Conheça a nossa lista de fundos",
                backgroundImage:"assets/images/fundos-de-investimento-bg.jpg"
            }
        })
    .otherwise({
        redirectTo: '/lista-fundos-de-investimento'
    });

    $locationProvider.html5Mode(true);
    $locationProvider.hashPrefix('#');
});
