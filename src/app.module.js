const App = 
    angular
    .module("OramaApp", ['ngRoute', 'ngSanitize', 'ngMessages'])
    .config(config)

function config($httpProvider, $qProvider) {

    $qProvider.errorOnUnhandledRejections(false);

    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
    $httpProvider.defaults.headers.put['Content-Type'] = 'application/x-www-form-urlencoded';
    $httpProvider.defaults.headers.patch['Content-Type'] = 'application/x-www-form-urlencoded';
    $httpProvider.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
}


App.controller('OramaAppController', function($scope, $rootScope, $route) {
    this.$route = $route;
});