const GET_LIST_URI = "https://s3.amazonaws.com/orama-media/json/fund_detail_full.json?limit=1000&offset=0&serializer=fund_detail_full";
const APPLY_FUNDS_URI = "https://6009ae91778d1a0017792d29.mockapi.io/test/1/recovery";


App.service('InvestmentFundsService', class InvestmentFundsService{

    constructor($http, $q, AlertModal) {
        'ngInject';
        this.$http = $http;
        this.$q = $q;
        this.AlertModal = AlertModal;
    }

    getList = _ => this._getAPI(GET_LIST_URI);

    apply = fundId => this._postAPI(APPLY_FUNDS_URI, {fundId})

    _getAPI = (uri, paramsData = {}) => this.$q((resolve, reject) => {
        this.$http
            .get(uri, {params: paramsData})
            .then(response => resolve(response.data))
            .catch(error => {
                this.catchApiError(error);
                reject(error)
            })
    })

    _postAPI = (uri, params = {}) => this.$q((resolve, reject) => {
        this.$http
            .post(uri, params)
            .then(response => resolve(response.data))
            .catch(error => {
                this.catchApiError(error);
                reject(error)
            })
    })

    catchApiError = error => {
        const message = error.data.message || "Ops... Ocorreu um erro inesperado, por favor tente novamente";
        this.AlertModal
            .show({message, closeButton:true})
    }

})
    