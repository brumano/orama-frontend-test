App.component('bannerHeader', {
        bindings: {
            title: '@',
            subTitle: '@?',
            backgroundImage: '@',
        },
        controllerAs: 'vm',
        templateUrl: 'components/banner-header/banner-header.view.html',
        controller: function BannerHeaderController() {},
    });
