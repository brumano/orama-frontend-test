App.component('alertModal', {
        controllerAs: 'vm',
        templateUrl: 'components/alert-modal/alert-modal.view.html',
        controller: function alertModalController($scope) {

            $scope.$on('show-alert-modal', (event, data) => {

                const {title, message, buttonText} = data;

                $scope.title = title;
                $scope.message =  message;
                $scope.buttonText = buttonText;

                const modal = new Foundation.Reveal($('#alert-modal'));
                modal.open();
            });
        },
    });
