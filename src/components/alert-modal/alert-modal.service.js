App.service('AlertModal', class InvestmentFundsService{

    constructor($rootScope) {
        'ngInject';
        this.$rootScope = $rootScope;
    }

    show(title, message, buttonText = 'Ok'){
        const data = {title, message, buttonText}
        this._rootScope.$broadcast('show-alert-modal', data)
    } 

})